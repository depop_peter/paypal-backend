package com.depop.backend

import akka.actor.{ActorSystem, Props}
import akka.io.IO
import akka.pattern.ask
import akka.util.Timeout
import com.braintreegateway.{BraintreeGateway, Environment}
import com.depop.backend.api.{PublicApi, ServiceActor}
import com.depop.backend.config.{AppConfig, BrainTreeConfig}
import com.depop.backend.services.BillingAgreementService
import com.typesafe.config.ConfigFactory
import spray.can.Http

import scala.concurrent.duration._

class Core extends App {
  implicit val system = ActorSystem("paypal-backend")

  val config = ConfigFactory.load()
  val appConfig = new AppConfig(config.getConfig("app"))
  val brainTreeConfig = new BrainTreeConfig(config.getConfig("braintree"))

  val gateway = new BraintreeGateway(Environment.SANDBOX, brainTreeConfig.merchantId, brainTreeConfig.publicKey, brainTreeConfig.privateKey)

  val billingAgreementService = new BillingAgreementService(gateway)

  val routes = new PublicApi(billingAgreementService).publicRoutes
  val service = system.actorOf(Props(new ServiceActor(routes)), "paypal-service")

  implicit val timeout = Timeout(5.seconds)
  IO(Http) ? Http.Bind(service, interface = "localhost", port = appConfig.publicPort)
}
