package com.depop.backend.api



import com.depop.backend.services.BillingAgreementService
import spray.routing.directives.PathDirectives

class PublicApi(billingAgreementService: BillingAgreementService) extends PathDirectives {
  val publicRoutes =
    pathPrefix("v1") {
      new TokenService(billingAgreementService).route
    }
}
