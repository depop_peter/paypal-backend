package com.depop.backend.api


import akka.actor.{Actor, ActorContext}
import spray.routing.{HttpService, Route}

class ServiceActor(routes: Route) extends Actor with HttpService {
  implicit override def actorRefFactory: ActorContext = context
  override def receive: Receive = runRoute(routes)
}
