package com.depop.backend.api

import com.depop.backend.services.BillingAgreementService
import spray.http.HttpHeaders.RawHeader
import spray.http.StatusCodes
import spray.routing._

class TokenService(billingAgreementService: BillingAgreementService) extends Directives {
  val route =
    path("billing-agreement") {
      post {
        val token = billingAgreementService.generateClientToken()
        respondWithHeader(RawHeader("PayPal-Token", token)) {
          respondWithStatus(StatusCodes.NoContent) {
            complete("")
          }
        }
      }
    }
}