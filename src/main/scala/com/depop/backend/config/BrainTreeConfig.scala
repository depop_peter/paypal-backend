package com.depop.backend.config

class BrainTreeConfig(config: com.typesafe.config.Config) {
  val merchantId: String = config.getString("merchant-id")
  val publicKey: String = config.getString("public-key")
  val privateKey: String = config.getString("private-key")
}
