package com.depop.backend.config

class AppConfig(config: com.typesafe.config.Config) {
  val publicPort = config.getInt("public-port")
}
