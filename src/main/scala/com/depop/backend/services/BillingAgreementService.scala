package com.depop.backend.services

import com.braintreegateway.BraintreeGateway

class BillingAgreementService(gateway: BraintreeGateway) {
  def generateClientToken(): String = {
    gateway.clientToken().generate()
  }
}
