package com.depop.test

import com.braintreegateway.{BraintreeGateway, ClientTokenGateway}
import org.specs2.mock.Mockito
object Stubs extends Mockito {
  val generatedToken: String = "generatedToken"
  val clientTokenStub = smartMock[ClientTokenGateway]
  val gatewayStub = smartMock[BraintreeGateway]

  gatewayStub.clientToken() returns clientTokenStub
  clientTokenStub.generate() returns generatedToken
}
