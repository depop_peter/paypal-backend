package com.depop.backend.services


import org.specs2.mock.Mockito
import org.specs2.mutable.Specification
import org.specs2.specification.Scope
import com.depop.test.Stubs


class BillingAgreementServiceSpec extends Specification with Mockito {
  trait BillingAgreementServiceScope extends Scope {
    val gatewayStub = Stubs.gatewayStub
    val billingAgreementService = new BillingAgreementService(gatewayStub)
  }
  "generateClientToken" should {
    "generate a client token using BrainTree" in new BillingAgreementServiceScope {
      billingAgreementService.generateClientToken() === Stubs.generatedToken
    }
  }
}
